package priyaworks;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

public class Test1 {

    static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
        SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<>(new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                int res = o2.getValue().compareTo(o1.getValue());
                return res != 0 ? res : 1;
            }
        });
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }


    public static void main(String arg[]) {

        TreeSet<LocalDate> treeSet = new TreeSet<>();
        TreeMap<String, Integer> wordMap1 = new TreeMap<>();
        TreeMap<String, Integer> wordMap2 = new TreeMap<>();
        LocalDate startDate = null;
        LocalDate consStartDate = null;
        LocalDate tempDate = null;
        LocalDate tempDate2 = null;

        int countCons = 0, maxCons = 0;

        int name1Count = 0, name2Count = 0;
        int talking = -1, i = 0;
        String str = "";
        String name1 = "Priya", name2 = "Sreeraj K";
        try {
            BufferedReader br = new BufferedReader(new FileReader("chat.txt"));

            while (true) {
                str = br.readLine();
                i++;
                if (str == null) {
                    break;
                } else if (str.compareTo("") == 0) {
                    continue;
                }
                if (Character.isDigit(str.charAt(0))) {
                    try {
                        String dd = str.substring(0, 2);
                        String mm = str.substring(3, 5);
                        String yy = str.substring(6, 10);

                        LocalDate d = LocalDate.of(Integer.parseInt(yy), Integer.parseInt(mm), Integer.parseInt(dd));
                        if (startDate == null) {
                            startDate = d;
                            consStartDate = d;
                            tempDate = LocalDate.of(d.getYear(), d.getMonth(), d.getDayOfMonth());
                            countCons = 1;
                            maxCons = 1;

                        } else {
                            tempDate = tempDate.plusDays(1);
                            if (tempDate.compareTo(d) == 0) {
                                countCons++;
                            } else if (tempDate.compareTo(d) > 0) {
                                if (maxCons < countCons) {
                                    maxCons = countCons;
                                    consStartDate = tempDate2;
                                }
                                countCons = 1;
                                tempDate = LocalDate.of(d.getYear(), d.getMonth(), d.getDayOfMonth());
                                tempDate2 = LocalDate.of(d.getYear(), d.getMonth(), d.getDayOfMonth());
                            } else {
                                tempDate = LocalDate.of(d.getYear(), d.getMonth(), d.getDayOfMonth());
                            }
                        }
                        treeSet.add(d);
                    } catch (NumberFormatException e) {
                        continue;
                    }
                } else {
                    //continuation of previous message
                }

                if (str.contains(name1 + ":")) {
                    int startIndex = str.indexOf(name1);
                    str = str.substring(startIndex + name1.length() + 2);
                    name1Count++;
                    talking = 1;
                } else if (str.contains(name2 + ":")) {
                    int startIndex = str.indexOf(name2);
                    str = str.substring(startIndex + name2.length() + 2);
                    name2Count++;
                    talking = 2;

                }

                String subStrings[] = str.replaceAll("<Media omitted>", "").replaceAll("\\.+|\\?+|\\*+", " ").split(" +");
                for (String s : subStrings) {
                    s = s.toLowerCase();
                    switch (talking) {
                        case 1:
                            wordMap1.put(s, wordMap1.getOrDefault(s, 0) + 1);
                            break;
                        case 2:
                            wordMap2.put(s, wordMap1.getOrDefault(s, 0) + 1);
                            break;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("No of days : " + treeSet.size());
        System.out.println("No Of Messages : " + String.valueOf(i - 1));
        System.out.println(name1 + " : " + name1Count + " messages sent");
        System.out.println(name2 + " : " + name2Count + " messages sent\n\n");
        System.out.println(name1 + " Best 20 words");
        printBest(wordMap1, 20);
        System.out.println("\n\n" + name2 + " Best 20 words");
        printBest(wordMap2, 20);
        System.out.println("Consecutive Days : " + maxCons + " Started At: " + consStartDate);


    }


    public static void printBest(Map<String, Integer> wordMap, int max) {
        SortedSet<Map.Entry<String, Integer>> x = entriesSortedByValues(wordMap);
        Iterator<Map.Entry<String, Integer>> iterator = x.iterator();
        int j = 0;
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> k = iterator.next();
            System.out.println(k.getKey() + "::" + k.getValue());
            j++;
            if (j > max) {
                break;
            }
        }
    }
}
