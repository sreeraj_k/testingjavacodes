package redis;

import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * Created by sreeraj on 14/7/15.
 */
public class RedisInfo {

    public static void main(String arg[]) {

        Jedis jedis = new Jedis("107.178.219.43", 5866, 1000);
        jedis.auth("ZTNiMGM0NDI5OGZjMWMxNDlhZmJmNGM4OTk2ZmI5");
        Set<String> keys = jedis.keys("http*");
        int i = 0;
        for (String key : keys) {
            System.out.println((i*1.0)*100/keys.size()+ "% ");
            jedis.hset(key, "time", "");
            i++;
        }
        System.out.println("i = " + i);
    }
}
