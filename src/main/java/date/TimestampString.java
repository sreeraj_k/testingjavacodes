package date;

import java.util.Date;

/**
 * Created by sreeraj on 17/8/15.
 */
public class TimestampString {

    public static void main(String arg[]) {

        long t = new Date().getTime();
        String s = Long.toString(t);
        System.out.println(s);
    }
}
