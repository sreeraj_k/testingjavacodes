package rssparse.test;

import rssparse.model.Feed;
import rssparse.model.FeedMessage;
import rssparse.read.RSSFeedParser;

/**
 * Created by sreeraj on 5/5/15.
 */
public class ReadTest {
    public static void main(String[] args) {
        RSSFeedParser parser = new RSSFeedParser("http://rss.cnn.com/rss/cnn_topstories.rss");
        Feed feed = parser.readFeed();
        System.out.println(feed);
        for (FeedMessage message : feed.getMessages()) {
            System.out.println(message);

        }

    }
}
