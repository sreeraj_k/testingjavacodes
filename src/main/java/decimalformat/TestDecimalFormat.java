package decimalformat;

import java.text.DecimalFormat;

/**
 * Created by sreeraj on 16/8/15.
 */
public class TestDecimalFormat {

    public static void main(String arg[]) {
        String code = "";
        for (int i = 0; i < 20; i++) {
            code += new DecimalFormat("00").format(i);
        }
        System.out.println(code);

        for (int i = 0; i < code.length(); i+=2) {
            String b = code.substring(i, i+2);
            System.out.println(Integer.parseInt(b));

        }
    }
}
