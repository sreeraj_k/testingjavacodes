package datastructures;

import java.util.Stack;

/**
 * Created by sreeraj on 27/9/15.
 */
public class StackQueueDemo {
    public static void main(String arg[]){

        Stack<Integer> st = new Stack<>();
        st.push(1);
        st.push(2);
        st.push(3);
        while(!st.isEmpty()) {
            System.out.println(st.pop());
        }
    }
}
