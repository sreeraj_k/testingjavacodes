package producerconsumer;

/**
 * Created by sreeraj on 27/8/15.
 */
public class Test {

    public static void main(String arg[]){

        Handler h = new Handler();
        Consumer c = new Consumer(h);
        Producer p = new Producer(h);
        c.start();
        p.start();
    }
}
