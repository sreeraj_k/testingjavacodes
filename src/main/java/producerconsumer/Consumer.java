package producerconsumer;

public class Consumer extends Thread{

    private Integer value;
    private Handler handler;
    public Consumer(Handler handler ){
        this.handler = handler;
    }

    public void run(){

        for(int i=0;i<10;i++){
            value = handler.get();
            System.out.println("Consumed " + value);
        }
    }
}
