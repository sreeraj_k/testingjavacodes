package producerconsumer;

public class Handler {

    private boolean available = false;
    private Integer content;

    synchronized public void put(Integer value){
        while(available == true){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        content = value;
        System.out.println("Produced " + content);
        available = true;
        notifyAll();
    }

    synchronized public Integer get(){
        while(available == false){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        available = false;
        notifyAll();
        return content;
    }

}
