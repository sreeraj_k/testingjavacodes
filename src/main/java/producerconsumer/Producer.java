package producerconsumer;

public class Producer extends Thread{

    private Handler handler;

    public Producer(Handler handler){
        this.handler = handler;
    }

    public void run(){

        for(int i=0;i<10;i++){
            handler.put(i);
        }

    }
}
